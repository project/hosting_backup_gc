Hosting Backup Garbage Collection
=================================

This is an add-on module for the Aegir hosting system:
http://aegirproject.org

Introduction
------------

This module allows you to specify how long Aegir should retain backups for. You
can specify, for example, that after a month, only one backup a week should be
kept. You can specify as many of these rules as you want, so you could keep
hourly backups for a day, daily backups for two weeks and monthly backups
thereafter.

Currently it applies to all sites hosted by Aegir, and there is no way to
exclude specific sites.

Installation
-----------

1. Install as any other Drupal module into your hostmaster site.
2. Enable the feature in the 'experimental' section of 'admin/hosting' page.

Configuration
-------------

1. Settings are available at 'admin/hosting/backup_gc' and allow you to choose
the numbers of backups to keep after specified periods of time.

About the Developers
--------------------

This project is currently maintained by developers at ComputerMinds - visit us
at http://www.computerminds.co.uk. We at ComputerMinds pride ourselves on
offering quality [Drupal training](http://www.computerminds.co.uk/drupal-training),
[Drupal development](http://www.computerminds.co.uk/drupal-development) and
[Drupal consulting](http://www.computerminds.co.uk/drupal-consulting). Go Drupal!
